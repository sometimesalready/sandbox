angular.module('moneyPie').factory('chartDrawer', ()=> {
    let clearChart = (context)=> {
        let width = context.canvas.width;
        let height = context.canvas.height;
        context.clearRect(0, 0, width, height);
        context.strokeStyle = 'black'
        context.strokeRect(0, 0, width, height);
    };
    let drawChart = (context, data)=> {
        clearChart(context);
        data.forEach(data=> {
            drawSector(context, data.startAngle, data.endAngle);
        });
    }
    let drawSector = (context, startAngle, finishAngle)=> {
        const center = {x: context.canvas.height / 2, y: context.canvas.height / 2};
        const RADIUS = Math.min(context.canvas.height, context.canvas.width) / 3;
        let BEGIN_AT = -Math.PI / 2;
        context.beginPath();
        context.moveTo(center.x, center.y);
        context.arc(center.x, center.y, RADIUS, BEGIN_AT + startAngle, BEGIN_AT + finishAngle);
        context.closePath();
        context.stroke();
    };
    return {
        drawChart,
        clearChart
    }
})