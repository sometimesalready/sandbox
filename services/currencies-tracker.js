angular.module('moneyPie')
    .factory('currencyTracker', ['$http', function ($http) {
        const currenciesNames = ['USD', 'EUR', 'CAD'];
        const URL =
            '//query.yahooapis.com/v1/public/yql?q=select * from '+
            `yahoo.finance.xchange where pair in ("${currenciesNames.join('USD","')+'USD'}")&format=json&`+
            'env=store://datatables.org/alltableswithkeys&callback=JSON_CALLBACK';

        let currencies = [];
        
        var refresh = function () {
            $http.jsonp(URL).then(function (response) {
                response.data.query.results.rate.forEach(function (rate, index) {
                    var currency = rate.Name.split('/')[0];
                    currencies.push({name: currency, rate: window.parseFloat(rate.Rate), id:index});
                });
            });
        };

        refresh();

        return {
            currencies
        };
    }]);

