angular.module('moneyPie')
    .factory('expensesKeeper', ()=> {
        let expenses = [];
        return {
            expenses,
            add: expense=>expenses.push(Object.assign({}, expense))
        }
    })