angular.module('moneyPie').component('myLabel', {
    transclude: true,
    templateUrl: 'views/templates/my-label.html',
    bindings: {
        text: '@',
        id: '@inputId'
    }
});