angular.module('moneyPie')
    .component('addExpenseForm', {
        templateUrl: '/views/templates/add-expense-form.html',
        controller: function (expensesKeeper, currencyTracker) {
            Object.assign(this, {
                expense: {},
                add: ()=> {
                    expensesKeeper.add(Object.assign({}, this.expense));
                },
                onCurrencySelect: currency=>this.expense.currency = currency
            })
        }
    });