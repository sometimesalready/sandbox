angular.module('moneyPie')
    .directive('label', ($compile)=>({
        restrict: 'A',
        transclude: 'element',
        link: (scope, el, attrs, ctrl, transclude)=> {
            transclude(scope, (clone)=> {
                let container = angular.element('<div></div>');
                let label = `<label for="${clone.attr('id')}">${attrs['label']}</label>`;
                container.append(label);

                clone.removeAttr('label');
                let template = $compile(clone);
                container.append(template(scope));
                el.after(container)
            });
        }
    }))