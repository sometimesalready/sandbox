{
    let controller = class {
        constructor($scope, currencyTracker) {
            this.currencyTracker = currencyTracker;
            this.$scope = $scope;
        }

        $onChanges(changes) {
            if (changes.selectedValue.currentValue !== undefined) this.selected = this.selectedValue.toString();
        }

        $onInit() {
            this.currencies = this.currencyTracker.currencies;
            this.unwatchFunctions = [];
            let unwatch = this.$scope.$watch(()=>this.selected, (value, prevValue)=> {
                if (value === prevValue) return;
                this.onSelect({
                    currency: this.currencies.reduce((acc, item)=> {
                        if (item.id === Number(value)) acc = item;
                        return acc;
                    }, null)
                });
            });
            this.unwatchFunctions.push(unwatch);
        }

        $onDestroy() {
            this.unwatchFunctions.forEach(unwatch=>unwatch());
        }
    };
    angular.module('moneyPie').component('currencySelect', {
        templateUrl: 'views/templates/currency-select.html',
        bindings: {
            selectedValue: '<',
            id: '@selectId',
            onSelect: '&'
        },
        controller
    })
}