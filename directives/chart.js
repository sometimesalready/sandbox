angular.module('moneyPie')
    .directive('chart', (expensesKeeper, currencyTracker)=> {
        const defaults = {
            height: 300,
            width: 300
        };

        return {
            template: '<canvas></canvas>',
            scope: {},
            controllerAs: '$ctrl',
            replace: true,
            controller: function (expensesKeeper, $scope, chartDrawer) {
                $scope.$watch(()=>expensesKeeper.expenses, (expenses)=> {
                    if (!expenses.length) return chartDrawer.clearChart(this.context);
                    let total = expenses.reduce((acc, expense)=>acc + expense.amount*expense.currency.rate, 0);
                    let currentAngle = 0;
                    let data = expenses.map(expense=> {
                        let angle = expense.amount*expense.currency.rate / total * 2 * Math.PI;
                        currentAngle += angle;
                        return {
                            startAngle: currentAngle - angle,
                            endAngle: currentAngle
                        };
                    });
                    chartDrawer.drawChart(this.context, data);
                }, true);
            },
            link: (scope, element, attrs, ctrl)=> {
                Object.keys(defaults).forEach(key=> {
                    attrs.$set(key, defaults[key] + 'px');
                });
                let context = ctrl.context = element[0].getContext('2d');
            }
        }
    })

