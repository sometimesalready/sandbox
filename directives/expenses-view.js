angular.module('moneyPie')
    .component('expensesView', {
        templateUrl: 'views/templates/expenses-list.html',
        controller: function (expensesKeeper) {
            this.expenses = expensesKeeper.expenses;
            this.changeCurrencyOnExpense = (expense, newCurrency)=>expense.currency = newCurrency;
        }
    });