module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        watch: {
            options: {
                livereload: true,
            },
            all: {
                files: ['**/*','!node_modules/**/*']
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch']);

};
